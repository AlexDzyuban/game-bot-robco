define(function() {


	'use strict';


	/**
	 * Neural Network
	 * @param {Object} currentIntellectStorage
	 * @constructor
	 */
	function NeuralNetwork(currentIntellectStorage) {

		/**
		 * Current intellect config
		 * Can be saved previously or created now
		 * @type {Array}
		 */
		this.intellectStorage = currentIntellectStorage || [];

		/**
		 * Weights for intellect compensation
		 * @type {Object}
		 */
		this.weights = {
			turn: 10,
			win: 10,
			draw: 5,
			lose: -10
		};

	}


	/**
	 * Init Neural Network game/learn session
	 */
	NeuralNetwork.prototype.initSession = function () {

		/**
		 * Intellect fragment for current game session
		 * @type {Object}
		 */
		this.intellectFragment = {
			turns: [],
			weights: [],
			totalWeight: 0,
			wins: 0,
			draws: 0,
			loses: 0
		};

		/**
		 * Marker. Is new intellect fragment added
		 * @type {Boolean}
		 */
		this.isNewFragmentAdded = false;

		/**
		 * Turns counter for curent session
		 * @type {Number}
		 */
		this.intellectCounter = 0;

		/**
		 * Possible turns
		 * Will be detected for each game turn
		 * @type {Array}
		 */
		this.possibleTurns = [];

		/**
		 * Input data
		 * Will be populated for each game turn
		 * @type {Object}
		 */
		this.inputData = {};

		/**
		 * Selected cell index
		 * Will be used for game turn
		 * @type {Number}
		 */
		this.selectedIndex = 0;

	};


	/**
	 * Get current intellect
	 * @returns {Array}
	 */
	NeuralNetwork.prototype.getIntellect = function() {
		return this.intellectStorage;
	};


	/**
	 * Get random value
	 * For random decisions only
	 * @param {Number} min
	 * @param {Number} max
	 * @returns {Number}
	 */
	NeuralNetwork.prototype.getRandomValue = function(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	};

	/**
	 * Normalize a float value
	 * @param {Number} value
	 * @returns {Number}
	 */
	NeuralNetwork.prototype.float = function(value) {
		return parseFloat((value).toFixed(1)) || 0;
	};


	/**
	 * Take a decision
	 * Analyze curent game board and current intellect data
	 * Select required cell
	 * @param {Object} inputData
	 * @returns {Object}
	 */
	NeuralNetwork.prototype.takeDecision = function(inputData) {

		// Save input data
		this.inputData = inputData;

		// Filter possible turns
		this.detectPossibleTurns();

		// Select required cell index
		this.analyzeSituation();

		// Increment intellect counter
		this.intellectCounter += 1;

		// Return data for current turn
		return {
			cell: this.selectedIndex,
			params: this.intellectFragment
		};

	};


	/**
	 * Analize current turn
	 * Check intellect data or make random turn
	 */
	NeuralNetwork.prototype.analyzeSituation = function () {

		switch(true) {

			// Neural Network has one or more game sessions
			case this.intellectStorage.length > 1:

				// Find the best turn weight in intellect and try to select index
				this.findBestTurnWeight();
				this.tryToSelectIndex();
				break;

			// Neural Network has one game session. Filtering is not needed
			case this.intellectStorage.length === 1:

				// Try to select index
				this.tryToSelectIndex();
				break;

			// Empty intellect storage
			case !this.intellectStorage.length || this.isNewFragmentAdded:

				// Choose random cell
				this.selectedIndex = this.getRandomCellIndex();
				this.populateIntellectFragment();
				break;

		}

	};


	/**
	 * Find the best turn weight in intellect
	 */
	NeuralNetwork.prototype.findBestTurnWeight = function() {

		var step = this.intellectCounter;

		this.intellectStorage = this.intellectStorage.sort(function(prevFragment, nextFragment) {

			var prevWinRatio = parseFloat((prevFragment.wins / prevFragment.loses).toFixed(1)),
				nextWinRatio = parseFloat((nextFragment.wins / nextFragment.loses).toFixed(1)),
				prevTotalWeight = prevFragment.totalWeight,
				nextTotalWeight = nextFragment.totalWeight,
				prevWeight =  this.float((prevFragment.weights[step] * prevWinRatio) + prevTotalWeight),
				nextWeight =  this.float((nextFragment.weights[step] * nextWinRatio) + nextTotalWeight);

			switch (true) {
				case prevWeight < nextWeight: return 1;
				case prevWeight > nextWeight: return -1;
				default: return 0;
			}

		}.bind(this));

	};


	/**
	 * Try to select the best index
	 * If it is not available then create new intellect fragment
	 */
	NeuralNetwork.prototype.tryToSelectIndex = function() {

		var bestFragment = this.intellectStorage[0],
			selectedIndex = bestFragment.turns[this.intellectCounter];

		switch(true) {

			// Working with current new fragment
			case this.isNewFragmentAdded:
				this.selectedIndex = this.getRandomCellIndex();
				this.populateIntellectFragment();
				break;

			// Working with best filtered fragment
			case !this.isNewFragmentAdded && this.possibleTurns.indexOf(selectedIndex) !== -1:
				this.selectedIndex = selectedIndex;
				this.intellectFragment = bestFragment;
				break;

			// Create new fragment. Copy part of the best fragment + add new data
			case !this.isNewFragmentAdded && this.possibleTurns.indexOf(selectedIndex) === -1:
				this.intellectFragment = {
					turns: bestFragment.turns.slice(0, this.intellectCounter),
					weights:  bestFragment.weights.slice(0, this.intellectCounter),
					totalWeight: 0,
					wins: 0,
					draws: 0,
					loses: 0
				};
				this.selectedIndex = this.getRandomCellIndex();
				this.populateIntellectFragment();
				break;

		}

	};


	/**
	 * Add weight for current turn to intellect fragment
	 */
	NeuralNetwork.prototype.populateIntellectFragment = function() {
		this.intellectFragment.turns.push(this.selectedIndex);
		this.intellectFragment.weights.push(this.weights.turn - this.intellectCounter);
		this.isNewFragmentAdded = true;
	};


	/**
	 * Get random cell index
	 * Select random cell index if intellect doesn't have data for current turn
	 * @returns {Number}
	 */
	NeuralNetwork.prototype.getRandomCellIndex = function() {
		return this.possibleTurns[this.getRandomValue(0, this.possibleTurns.length - 1)];
	};


	/**
	 * Detect possible turns
	 */
	NeuralNetwork.prototype.detectPossibleTurns = function() {

		var currentCombination = this.inputData.currentGameCombination,
			index;

		// Clear previous possible turns
		this.possibleTurns = [];

		// Collect possible turns indexes
		for(index = 0; index < currentCombination.length; index++) {
			if(!currentCombination[index]) {
				this.possibleTurns.push(index);
			}
		}

	};


	/**
	 * Finish session
	 * Summarize game session results
	 * Encourage or punish Neural Network
	 * @param {Object} sessionData
	 * @returns {Array}
	 */
	NeuralNetwork.prototype.finishSession = function(sessionData) {

		// If updating of current fragment is needed
		if(!this.isNewFragmentAdded) {
			this.intellectFragment = this.intellectStorage[0];
		}

		// Set weights. Encourage or punish Neural Network
		switch(sessionData.result) {

			case 'win':
				this.intellectFragment.wins += 1;
				this.intellectFragment.totalWeight += this.weights.win;
				break;

			case 'draw':
				this.intellectFragment.draws += 1;
				this.intellectFragment.totalWeight += this.weights.draw;
				break;

			case 'lose':
				this.intellectFragment.loses += 1;
				this.intellectFragment.totalWeight += this.weights.lose;
				break;

		}

		// Add intellect fragment to intellect storage if needed
		this.isNewFragmentAdded && this.intellectStorage.push(this.intellectFragment);

	};


	return NeuralNetwork;

});