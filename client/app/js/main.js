/**
 * Reguire config
 */
require.config({

	paths: {
		'angular': '../lib/angular.min',
		'angularRoute': '../lib/angularRoute',
		'app': 'app',
		'neuralNetwork': '../../neuralNetwork/neuralNetwork'
	},

	shim: {

		'angular': {
			exports: 'angular'
		},

		'angularRoute': {
			deps: ['angular']
		},

		'app': {
			deps: ['angular', 'angularRoute']
		}
	}

});


/**
 * Bootstrap application
 */
require(['app'], function() {

	'use strict';

	angular.bootstrap(document, ['gamebotRobcoApp']);
});