define(['./_module'], function (controllers) {


	'use strict';


	/**
	 * Loading page controller
	 */
	controllers.controller('loadingCtrl', function ($scope, $location) {

		/**
		 * Controller scope
		 * @type {Object}
		 */
		$scope.loading = {

			preloaderTimeout: 5800,
			introMusic: 'app/music/robco-intro.mp3',

			/**
			 * Show loader
			 */
			showLoader: function() {
				setTimeout(function() {

					// Make redirect to Homepage
					$location.path('/home').replace();
					$scope.$apply();

				}, $scope.loading.preloaderTimeout);
			}
		};

		// Init loader
		$scope.loading.showLoader();

	});

});