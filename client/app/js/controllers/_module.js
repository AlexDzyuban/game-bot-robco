define(function() {


	'use strict';


	/**
	 * Controllers module
	 */
	return angular.module('gamebotRobcoApp.controllers', []);

});