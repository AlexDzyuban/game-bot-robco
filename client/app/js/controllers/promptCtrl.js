define(['./_module'], function (controllers) {


	'use strict';


	/**
	 * Prompt messages conroller
	 */
	controllers.controller('promptCtrl', function($scope) {

		/**
		 * Controller scope
		 * @type {Object}
		 */
		$scope.prompt = {

			/**
			 * Prompt params
			 */
			title: '',
			message: '',
			submitText: '',
			cancelText: '',
			isActive: false,
			isConfirmPrompt: false,
			showingDelay: 1500,

			sounds: {
				win: 'app/music/robco-win-effect.mp3',
				lose: 'app/music/robco-lose-effect.mp3'
			},

			/**
			 * Handler on click on OK button
			 * Can be added dynamically
			 */
			agreeAction: function() {},

			/**
			 * Hide prompt
			 */
			hide: function() {
				this.isActive = false;
			},

			/**
			 * Set required prompt params depending on type
			 * @param {String} type
			 */
			checkPromptType: function(type) {

				switch(type) {
					case 'confirm':
						this.isConfirmPrompt = true;
						this.submitText = 'Yes';
						this.cancelText = 'No';
						break;
					case 'alert':
						$scope.prompt.isConfirmPrompt = false;
						this.submitText = 'Ok';
						this.cancelText = '';
						break;
				}

			},

			/**
			 * Activate sound effect
			 */
			activateSoundEffect: function(type) {

				var player = document.getElementById('robco-keyboard-player');

				if(!type) {
					return;
				}

				player.src = this.sounds[type];
				player.onloadeddata = function() {
					player.volume = 0.5;
					player.play();
				}
			},

			/**
			 * Listen show prompt event
			 * Show prompt with required config
			 */
			listenShowPromptEvent: function() {

				$scope.$on('showPromptEvent', function(event, config) {
					// Set prompt with delay for animation
					setTimeout(function() {
						this.title = config.title;
						this.message = config.message;
						this.checkPromptType(config.type);
						this.agreeAction = config.agreeAction;
						this.isActive = true;
						this.activateSoundEffect(config.sound);
						$scope.$apply();
					}.bind(this), config.delay || this.showingDelay);

				}.bind(this));

			}

		};


		// Listen show prompt event
		$scope.prompt.listenShowPromptEvent();

	});

});