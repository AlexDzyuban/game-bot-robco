define(['./_module'], function (controllers) {


	'use strict';


	/**
	 * Homepage controller
	 */
	controllers.controller('homeCtrl', function ($scope) {

		/**
		 * Controller scope
		 * @type {Object}
		 */
		$scope.homepage = {
			heading: 'Welcome to ROBCO Industries (TM) Termlink',
			subHeading: '"We\'re in the business of happiness."',
			menuConfig: [{
				page: '/play-with-gamebot',
				text: 'Play with Game Bot',
				isConfigRequestNeeded: true
			}, {
				page: '/learn-gamebot',
				text: 'Teach Game Bot'
			}, {
				page: '/change-gamebot-config',
				text: 'Choose Game Bot Personality Mode'
			}]
		};

	});

});