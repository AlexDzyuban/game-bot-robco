define(['./_module'], function (controllers) {


	'use strict';


	/**
	 * Config page controller
	 */
	controllers.controller('configCtrl', function ($scope, configsStorageSrv) {

		/**
		 * Controller scope
		 * @type {Object}
		 */
		$scope.configPage = {

			/**
			 * Config page params
			 */
			heading: 'Robco Game Bot Personality Modes',
			subHeading: '"You can choose, import or export Personality Mode."',
			importConfigLink: 'Import new Personality Mode...',
			exportConfigLink: 'Export current Personality Mode...',
			savedConfigs: [],

			/**
			 * Create configs list
			 * Use saved configs
			 * @param {Object} config
			 */
			initConfigsList: function(config) {
				var savedConfigs = configsStorageSrv.getSavedConfigs() || {},
					activeConfigName = configsStorageSrv.getActiveConfigName(),
					cfg,
					index = 0;

				// Clear all configs
				this.savedConfigs = [];

				// Add configs to list
				for(cfg in savedConfigs) {
					if(savedConfigs.hasOwnProperty(cfg)) {
						index++;
						this.savedConfigs.push({
							name: 'Personality Mode #' + index + ' (' + cfg + ');',
							data: savedConfigs[cfg].data,
							storageIndex: cfg,
							isActive: cfg === activeConfigName
						});
					}
				}

				// Update view with configs list if needed
				config && config.redrawList && $scope.$apply();
			}
		};

		// Create configs list
		$scope.configPage.initConfigsList();

	});

});