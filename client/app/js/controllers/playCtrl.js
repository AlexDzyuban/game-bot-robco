define(['./_module'], function (controllers) {


	'use strict';


	/**
	 * Play and Learn pages controller
	 */
	controllers.controller('playCtrl', function ($scope, $rootScope, randomValueSrv, promptSrv, configsStorageSrv) {

		/**
		 * Set active intellect for current game session
		 * "Learn Mode" -> Create NEW empty intellect
		 * "Play Mode" -> Use CURRENT intellect
		 */
		var activeConfig = configsStorageSrv.getActiveConfig() || {data: []},
			isLearnMode = location.href.indexOf('learn') !== -1,
			activeIntellectStorage = isLearnMode ? [] : activeConfig.data;

		/**
		 * Create Neural Networks
		 * @type {Object}
		 */
		$scope.neuralNetwork = new $rootScope.NeuralNetwork(
			activeIntellectStorage
		);

		/**
		 * Controller scope config
		 * @type {Object}
		 */
		$scope.config = {

			/**
			 * Pages models
			 * @type {Object}
			 */
			playPage: {
				heading: 'Play game with Robco Game Bot',
				subHeading: '"Try to win!"'
			},
			learnPage: {
				heading: 'Play game and teach Robco Game Bot',
				subHeading: '"You can save Game Bot Personality Mode."'
			},

			/**
			 * Game field model
			 * @type {Object}
			 */
			gameField: {
				cells: [],
				disabled: false
			},

			/**
			 * Information field config
			 * @type {Object}
			 */
			infoField: {
				turn: '',
				turns: [
					'Your turn',
					'Game Bot\'s turn'
				],
				turnsLog: []
			},

			/**
			 * Game session notification
			 */
			notifications: {
				winTitle: 'You lose',
				loseTitle: 'You win',
				drawTitle: 'It\'s a draw',
				learnMoreTitle: 'Attention',
				successTitle: 'Success',
				playAgainMessage: 'Do you want to play again?',
				learnMoreMessage: 'Game Bot is not ready. ' +
								  'Please, play more. ' +
								  'To save a Personality Mode he needs 5 or more combinations.',
				savedConfigMessage: 'New Personality Mode has been saved successfully.',
				confirm: 'confirm',
				alert: 'alert'
			},

			/**
			 * Create new game session
			 * Create new Neural Network "learn" or "play" session
			 */
			createGameSession: function() {

				// Init game session
				$scope.gameSession = new $scope.GameSession();
				$scope.gameSession.init();

				// Init Neural Network learn session
				$scope.neuralNetwork.initSession();

			},

			/**
			 * Save current Personality Mode
			 */
			savePersonalityMode: function() {

				var configToSave = $scope.neuralNetwork.getIntellect(),
					minComboValue = 5;

				if(configToSave.length >= minComboValue) {

					// Save required config
					configsStorageSrv.saveConfig(configToSave, true);

					// Notify user
					promptSrv.showPrompt({
						title:  this.notifications.successTitle,
						message: this.notifications.savedConfigMessage,
						type: this.notifications.alert,
						delay: 1
					});

				} else {

					// Notify user. Neral Network needs more combinations
					promptSrv.showPrompt({
						title:  this.notifications.learnMoreTitle,
						message: this.notifications.learnMoreMessage,
						type: this.notifications.alert,
						delay: 1
					});

				}

			}

		};


		/**
		 * New Game Session
		 * @constructor
		 */
		$scope.GameSession = function() {

			/**
			 * Current turn
			 * false -> user's turn
			 * true -> Game Bot's turn
			 * @type {Boolean}
			 */
			this.isGameBotsTurn = false;

			/**
			 * Random Game bot's turn timeout
			 * [1500-2000ms]
			 * @returns {Number}
			 */
			this.gameBotTurnTimeout = function() {
				return randomValueSrv(1500, 2000);
			};

			/**
			 * Game over marker
			 * @type {Boolean}
			 */
			this.isGameOver = false;

			/**
			 * Turns params
			 */
			this.turnsCounter = 0;
			this.minTurnsCount = 5;
			this.cellsCount = 9;
			this.currentMarker = '';

			/**
			 * Game rules config
			 * @type {Array}
			 */
			this.gameRules = [
				[1, 1, 1, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 1, 1, 1, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 1, 1, 1],
				[1, 0, 0, 1, 0, 0, 1, 0, 0],
				[0, 1, 0, 0, 1, 0, 0, 1, 0],
				[0, 0, 1, 0, 0, 1, 0, 0, 1],
				[1, 0, 0, 0, 1, 0, 0, 0, 1],
				[0, 0, 1, 0, 1, 0, 1, 0, 0]
			];

			/**
			 * Current field state
			 * @type {Array}
			 */
			this.currentFieldState = [
				0, 0, 0, 0, 0, 0, 0, 0, 0
			]

		};


		/**
		 * Init New Game
		 */
		$scope.GameSession.prototype.init = function() {

			// Clear turns log
			$scope.config.infoField.turnsLog = [];

			// Add empty cells
			this.populateCells();

			// Detect first player's turn
			this.isGameBotsTurn = this.getFirstTurnOrder();

			// Game bot's first turn
			this.isGameBotsTurn && this.gameBotTurn();

		};


		/**
		 * Prepare game field
		 * Add cells from config
		 */
		$scope.GameSession.prototype.populateCells = function() {
			$scope.config.gameField.cells = [];

			for(var cell = 0; cell < this.cellsCount; cell++) {
				$scope.config.gameField.cells.push({
					dataMarker: '',
					itemWinner: false
				});
			}
		};


		/**
		 * Decide who will turn first
		 * @returns {Boolean}
		 */
		$scope.GameSession.prototype.getFirstTurnOrder = function() {
			var isGameBotsTurn = randomValueSrv(0, 1);

			$scope.config.infoField.turn = $scope.config.infoField.turns[isGameBotsTurn];

			return Boolean(isGameBotsTurn);
		};


		/**
		 * Add marker to game field
		 * On click on field cell
		 * @param {Number} index
		 */
		$scope.GameSession.prototype.makeTurn = function(index) {

			// If cell index is incorrect, cell is already marked, it's game bot's turn now or game is over
			if(typeof index !== 'number' ||
			   $scope.config.gameField.cells[index].dataMarker.length > 0 ||
			   this.isGameOver ||
			   $scope.config.gameField.disabled) {

				// Don't make a turn
				return;

			}

			// Add marker in required cell
			this.currentMarker = this.isGameBotsTurn ? 'o': 'x';
			$scope.config.gameField.cells[index].dataMarker = this.currentMarker;
			this.currentFieldState[index] = this.currentMarker;
			this.turnsCounter += 1;
			!this.isGameBotsTurn && this.updateTurnsLog({isUser: true});

			// Check current turn results
			switch(true) {

				// Victory -> Winning combination is matched
				case this.isWinningCombination():

					this.isGameOver = true;

					// Notify Neural Network
					$scope.neuralNetwork.finishSession({
						result: this.isGameBotsTurn ? 'win' : 'lose'
					});

					// Notify user
					promptSrv.showPrompt({
						title:  $scope.config.notifications[this.isGameBotsTurn ? 'winTitle' : 'loseTitle'],
						message: $scope.config.notifications.playAgainMessage,
						type: $scope.config.notifications.confirm,
						agreeAction: $scope.config.createGameSession,
						sound: this.isGameBotsTurn ? 'lose' : 'win'
					});

					break;

				// Draw -> All fields already marked
				case this.turnsCounter === this.cellsCount:

					this.isGameOver = true;

					// Notify Neural Network
					$scope.neuralNetwork.finishSession({
						result: 'draw'
					});

					// Notify user
					promptSrv.showPrompt({
						title: $scope.config.notifications.drawTitle,
						message: $scope.config.notifications.playAgainMessage,
						type: $scope.config.notifications.confirm,
						agreeAction: $scope.config.createGameSession,
						sound: 'win'
					});

					break;

				// Play next turn
				default:

					// Reverse player's config
					this.isGameBotsTurn = !this.isGameBotsTurn;
					$scope.config.infoField.turn = $scope.config.infoField.turns[this.isGameBotsTurn ? 1 : 0];

					// Game bot's turn
					this.isGameBotsTurn && this.gameBotTurn();

			}

		};


		/**
		 * Check winning combination
		 * @returns {Boolean}
		 */
		$scope.GameSession.prototype.isWinningCombination = function() {

			var rule,
				rules = this.gameRules,
				rulesLength = rules.length,
				ruleCell,
				winningCell,
				gameCell,
				currentGameCombination = [];

			// Don't check winning combination for small turns count
			if(this.turnsCounter < this.minTurnsCount) {
				return false;
			}

			// Iterate winning combinations
			for(rule = 0; rule < rulesLength; rule++) {

				// Iterate winning cells
				for(ruleCell = 0; ruleCell < rules[rule].length; ruleCell++) {
					winningCell = rules[rule][ruleCell];
					gameCell = $scope.config.gameField.cells[ruleCell].dataMarker;

					// Compare winning and game cells
					if(Boolean(winningCell) === Boolean(gameCell) &&
					   gameCell === this.currentMarker) {
						currentGameCombination[ruleCell] = 1;
					} else {
						currentGameCombination[ruleCell] = 0;
					}

				}

				// Compare winning and game combination
				if(currentGameCombination.toString() === rules[rule].toString()) {

					// Highlight winning line
					this.highlightWinningLine(currentGameCombination);

					return true;

				}

			}

		};


		/**
		 * Highlight winning line
		 * @param {Array} cells
		 */
		$scope.GameSession.prototype.highlightWinningLine = function (cells) {

			var cellsModel = $scope.config.gameField.cells,
				cell;

			// Highlight required cells
			for(cell = 0; cell < cells.length; cell++) {
				cells[cell] && (cellsModel[cell].itemWinner = true);
			}

		};


		/**
		 * Game bot's turn logic
		 */
		$scope.GameSession.prototype.gameBotTurn = function() {

			var decisionData;

			// Disable game field
			$scope.config.gameField.disabled = true;

			// Make turn
			setTimeout(function() {

				// Decide and select required cell
				decisionData = $scope.neuralNetwork.takeDecision({
					currentGameCombination: this.currentFieldState,
					gameTurnIndex: this.turnsCounter
				});

				// Enable game field
				$scope.config.gameField.disabled = false;

				// Make a turn
				this.makeTurn(decisionData.cell);

				// Update turns log
				this.updateTurnsLog(decisionData);

				// Redraw current View
				$scope.$apply();

			}.bind(this), this.gameBotTurnTimeout());

		};


		/**
		 * Update turns log
		 */
		$scope.GameSession.prototype.updateTurnsLog = function(data) {

			var params = data.params,
				combination,
				ratio,
				selectedCell;


			if(params) {
				selectedCell = 'Cell #' + (data.cell + 1) + ' selected.';
				combination = data.params.turns.map(function(value) {
					return value + 1;
				});
				combination = 'Possible combination - ' +
							  combination.join('-');
				ratio = 'Wins/Draws/Loses - ' +
						params.wins + '/' +
						params.draws + '/' +
						params.loses;
			}

			// Add item
			$scope.config.infoField.turnsLog.push({
				isUser: data.isUser,
				turnNumber: this.turnsCounter,
				combination: combination,
				ratio: ratio,
				selectedCell: selectedCell
			});

			// Scroll last item into view
			setTimeout(function() {
				var logList = document.getElementById('robco-turns-list');

				logList && (logList.scrollTop = 99999);
			}, 0);

		};


		// Create new game session and Neural Nework learn session
		$scope.config.createGameSession();

	});

});