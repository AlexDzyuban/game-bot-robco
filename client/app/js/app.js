define([
	'neuralNetwork',
	'controllers/_index',
	'directives/_index',
	'services/_index'
	], function (NeuralNetwork) {


	'use strict';


	/**
	 * Main gamebotRobcoApp module
	 */
	var gamebotRobcoApp = angular.module('gamebotRobcoApp', [
		'ngRoute',
		'gamebotRobcoApp.controllers',
		'gamebotRobcoApp.directives',
		'gamebotRobcoApp.services'
	]);


	/**
	 * Run application
	 * Extend $rootScope
	 * Add NeuralNetwork constructor
	 */
	gamebotRobcoApp.run(function($rootScope) {
		$rootScope.NeuralNetwork = NeuralNetwork;
	});


	/**
	 * Config
	 * Application routes setup
	 */
	gamebotRobcoApp.config(function($routeProvider) {

		$routeProvider

			.when('/loading', {
				controller: 'loadingCtrl',
				templateUrl: 'app/js/views/loading.html'
			})

			.when('/home', {
				controller: 'homeCtrl',
				templateUrl: 'app/js/views/home.html'
			})

			.when('/play-with-gamebot', {
				controller: 'playCtrl',
				templateUrl: 'app/js/views/play.html'
			})

			.when('/learn-gamebot', {
				controller: 'playCtrl',
				templateUrl: 'app/js/views/learn.html'
			})

			.when('/change-gamebot-config', {
				controller: 'configCtrl',
				templateUrl: 'app/js/views/config.html'
			})

			.otherwise({
				redirectTo: '/loading'
			});

	});


	return gamebotRobcoApp;

});