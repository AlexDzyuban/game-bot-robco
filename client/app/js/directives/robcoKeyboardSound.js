define(['./_module'], function (directives) {


	'use strict';


	/**
	 * Player object
	 * Will be created once
	 */
	var player;


	/**
	 * Activate keyboard sound on element interaction directive
	 */
	directives.directive('robcoKeyboardSound', function($compile, randomValueSrv) {

		return {
			link: function($scope, $element) {
				$element.on('mouseover click', function() {

					player = player || document.getElementById('robco-keyboard-player');
					player.src = 'app/music/robco-keyboard-' + randomValueSrv(1, 2) + '.mp3';
					player.onloadeddata = function() {
						player.volume = 0.1;
						player.pause();
						player.play();
					}

				});
			}
		};

	});

});