define(['./_module'], function (directives) {


	'use strict';


	/**
	 * Save current Personality Mode directive
	 * On Config page only
	 */
	directives.directive('robcoImportNewConfig', function(configsStorageSrv) {

		/**
		 * Configuration buttons nodes
		 */
		var controller,
			importFakeLink,
			exportFakeLink,
			exportFakeBase64Link,
			fileInput,

			/**
			 * Max file size (100Mb)
			 */
			maxFileSize = 1024 * 1024 * 100,

			/**
			 * Text parts for import/export
			 * @type {Object}
			 */
			textParts = {
				fileName: 'Personality Mode ',
				fileExtension: '.json',
				fileEncoding: 'data:text/json;charset=utf-8;base64,'
			},

			/**
			 * Methods to import/export files with configs
			 * @type {Object}
			 */
			file = {

				/**
				 * Export config
				 * Create .json file
				 */
				exportConfig: function() {
					var currentConfig = configsStorageSrv.getActiveConfig();

					if(!exportFakeBase64Link.length || !currentConfig || !currentConfig.data) {
						return;
					}

					try {

						// Add file name
						exportFakeBase64Link.attr(
							'download',
							textParts.fileName + '(' +
							configsStorageSrv.getActiveConfigName() +
							')' + textParts.fileExtension
						);

						// Add file data converted in base64
						exportFakeBase64Link.attr(
							'href',
							textParts.fileEncoding + btoa(JSON.stringify(currentConfig.data))
						);

						// Open Download window
						exportFakeBase64Link[0].click();

					} catch(e) {}
				},

				/**
				 * Import config
				 * Parse .json file
				 */
				importConfig: function() {

					var configFile = null,
						fileReader;

					// Get file
					try {
						configFile = fileInput[0].files[0];
						if(!configFile || configFile.size > maxFileSize) {
							return;
						}
					} catch(e) {
						return;
					}

					// Read file
					fileReader = new FileReader();
					fileReader.addEventListener('load', function(event) {

						// Save new data
						file.saveConfig(event.target.result);

					});
					fileReader.readAsText(configFile);

				},

				/**
				 * Save config from file
				 * Add new config to storage and config list
				 * @param {String} data
				 */
				saveConfig: function(data) {

					try {
						configsStorageSrv.saveConfig(
							JSON.parse(data)
						);
						controller.initConfigsList({
							redrawList: true
						});
					} catch(e) {}

				}

			};


		return {
			link: function($scope, $element) {

				controller = $scope.configPage;
				importFakeLink = $element.find('a').eq(0);
				exportFakeLink = $element.find('a').eq(1);
				exportFakeBase64Link = $element.find('a').eq(2);
				fileInput = $element.find('input');

				// Emulate file import on click on fake link
				importFakeLink.on('click', function() {
					fileInput[0] && fileInput[0].click();
				});

				// Export file on fake click on button
				exportFakeLink.on('click', file.exportConfig);

				// Import and parse file on file downloading
				fileInput.on('change', file.importConfig);

			}
		};

	});

});