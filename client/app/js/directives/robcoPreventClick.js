define(['./_module'], function (directives) {


	'use strict';


	/**
	 * Prevent default action on click
	 */
	directives.directive('robcoPreventClick', function() {

		return {
			link: function($scope, $element) {
				$element.on('click', function(event) {
					event.preventDefault();
				});
			}
		};

	});

});