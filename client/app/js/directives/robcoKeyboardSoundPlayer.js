define(['./_module'], function (directives) {


	'use strict';


	/**
	 * Activate keyboard sound on element interaction directive
	 */
	directives.directive('robcoKeyboardSoundPlayer', function() {
		return {
			template: '<audio id="robco-keyboard-player"><source src="" type="audio/mpeg"></audio>'
		};
	});

});