define(function() {


	'use strict';


	/**
	 * Directives module
	 */
	return angular.module('gamebotRobcoApp.directives', []);

});