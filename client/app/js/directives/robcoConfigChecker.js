define(['./_module'], function (directives) {


	'use strict';


	/**
	 * Check availability of current Personality Mode
	 * Don't open "Play" mode if Modes are absent or not activated
	 */
	directives.directive('robcoConfigChecker', function(configsStorageSrv, promptSrv) {

		/**
		 * Directive methods
		 * @type {Object}
		 */
		var directive = {

			// Controller scope
			scope: {},

			/**
			 * Notification messages
			 */
			message: {
				noModesTitle: 'Attention',
				noModesMsg: 'You don\'t have saved Personality Modes. ' +
							'Please, import one or teach Game Bot and save Personality Modes.',
				noActiveMsg: 'Please, select current Personality Mode. Game Bot needs brains.',
				alert: 'alert'
			},

			/**
			 * Check current Personality Mode
			 * Don't open "Play" mode if Modes are absent or not activated
			 * @param {Object} event
			 */
			checkCurrentConfig: function(event) {

				var isPlayLink = this.classList.contains('robco-check-config'),
					savedConfigs = configsStorageSrv.getSavedConfigs(),
					activeConfig = configsStorageSrv.getActiveConfig();

				switch (true) {

					// User doesn't have save Personality Modes
					case isPlayLink && !savedConfigs:
						event.preventDefault();
						promptSrv.showPrompt({
							title:  directive.message.noModesTitle,
							message: directive.message.noModesMsg,
							type: directive.message.alert,
							delay: 1
						});
						break;

					// User doesn't have active Personality Mode
					case isPlayLink && !activeConfig:
						event.preventDefault();
						promptSrv.showPrompt({
							title:  directive.message.noModesTitle,
							message: directive.message.noActiveMsg,
							type: directive.message.alert,
							delay: 1
						});
						break;

				}
			}

		};


		return {
			link: function($scope, $element) {
				directive.scope = $scope;
				$element.on('click', directive.checkCurrentConfig);
			}
		};

	});

});