define(['./_module'], function (directives) {


	'use strict';


	/**
	 * Choose current Personality Mode directive
	 * On Config page only
	 */
	directives.directive('robcoCurrentConfig', function (configsStorageSrv) {

		/**
		 * Current config service methods
		 * @type {Object}
		 */
		var service = {

			/**
			 * Remove active state for all configs
			 * @param {Object} configData
			 */
			deactivateConfigs: function(configData) {

				configData.savedConfigs = configData.savedConfigs.map(function(value) {
					value.isActive = false;
					return value;
				});

			},

			/**
			 * Choose current Personality Mode
			 * @param {Object} $scope
			 * @param {Object} $element
			 */
			activateRequiredConfig: function ($scope, $element) {

				$element.on('click', function () {

					// Set active config
					configsStorageSrv.setActiveConfig($scope.config.storageIndex);

					// Clear active state for all configs
					service.deactivateConfigs($scope.$parent.configPage);

					// Hightlight active config ib list
					$scope.config.isActive = true;
					$scope.$apply();

				});
			}
		};

		return {
			link: service.activateRequiredConfig
		};

	});

});