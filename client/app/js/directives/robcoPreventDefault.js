define(['./_module'], function (directives) {


	'use strict';


	/**
	 * Prevent default context menu showing directive
	 */
	directives.directive('robcoPreventDefault', function() {
		return {
			link: function($scope, $element) {
				$element.on('contextmenu', function(event) {
					event.preventDefault();
				});
			}
		};
	});

});