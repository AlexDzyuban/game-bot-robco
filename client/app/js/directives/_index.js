define([
	'./robcoPreventDefault',
	'./robcoKeyboardSoundPlayer',
	'./robcoKeyboardSound',
	'./robcoPreventClick',
	'./robcoImportNewConfig',
	'./robcoCurrentConfig',
	'./robcoConfigChecker'
], function () {});