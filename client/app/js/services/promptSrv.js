define(['./_module'], function (services) {


	'use strict';


	/**
	 * Get random value service
	 */
	services.service('promptSrv', function($rootScope) {

		return {

			/**
			 * Show prompt with required config
			 * @param {Object} config
			 */
			showPrompt: function(config) {
				$rootScope.$broadcast('showPromptEvent', config);
			}
		};

	});

});

