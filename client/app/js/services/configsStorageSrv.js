define(['./_module'], function (services) {


	'use strict';


	/**
	 * Get random value service
	 */
	services.service('configsStorageSrv', function() {

		var storage = {
			modes: 'personalityModes',
			activeMode: 'activePersonalityMode'
		};

		return {

			/**
			 * Get current date
			 * @returns {String}
			 */
			getCurrentDate: function() {

				var date = (new Date()).toString().split(' ');

				return date[2] + '-' + date[1] + '-' + date[3] + ' ' + date[4];

			},

			/**
			 * Get all saved configs from storage
			 * @returns {Array}
			 */
			getSavedConfigs: function() {

				try {
					return JSON.parse(localStorage.getItem(storage.modes)) || null;
				} catch(e) {
					return null;
				}

			},

			/**
			 * Get first active config name from storage
			 * @returns {String}
			 */
			getActiveConfigName: function() {
				return localStorage.getItem(storage.activeMode) || '';
			},

			/**
			 * Get first active config from storage
			 * @returns {Object}
			 */
			getActiveConfig: function() {

				var activeConfigName = this.getActiveConfigName(),
					savedConfigs = this.getSavedConfigs() || {};

				if(!activeConfigName) {
					return null;
				}

				return savedConfigs[activeConfigName] || null;

			},

			/**
			 * Set active config
			 * @param {String} name
			 */
			setActiveConfig: function(name) {
				localStorage.setItem(storage.activeMode, name);
			},

			/**
			 * Save new config to storage
			 * @param {Object} config
			 * @param {Boolean} isManuallySaved
			 */
			saveConfig: function(config, isManuallySaved) {

				try {

					var savedConfigs = this.getSavedConfigs() || {},
						configName = (isManuallySaved ? 'saved manually' : 'imported') + ' ' +
									  this.getCurrentDate() + ', ' + config.length + ' lvl.';

					// Add/Update new config
					savedConfigs[configName] = {
						data: config
					};

					// Update configs
					localStorage.setItem(
						storage.modes,
						JSON.stringify(savedConfigs)
					);

				} catch(e) {}

			}
		};

	});

});

