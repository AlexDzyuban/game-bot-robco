define(function() {


	'use strict';


	/**
	 * Services module
	 */
	return angular.module('gamebotRobcoApp.services', []);

});