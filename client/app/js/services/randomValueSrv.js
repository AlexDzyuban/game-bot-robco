define(['./_module'], function (services) {


	'use strict';


	/**
	 * Get random value service
	 */
	services.service('randomValueSrv', function() {

		return function(min, max) {
			return Math.floor(Math.random() * (max - min + 1)) + min;
		};

	});

});

